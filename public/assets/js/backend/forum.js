define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'forum/index',
                    add_url: 'forum/add',
                    edit_url: 'forum/edit',
                    del_url: 'forum/del',
                    multi_url: 'forum/multi',
                    table: 'forum',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'weigh',
                columns: [
                    [                   
                        { checkbox: true },
                        { field: 'id', title: __('Id') },
                        { field: 'category_id', title: __('Category_id') },
                        { field: 'category_ids', title: __('Category_ids') },
                        { field: 'type', title: __('Type'), searchList: { "hot": __('Hot'), "index": __('Index'), "recommend": __('Recommend'), "tom": __('Tom'), "aaa": __('Aaa') }, operate: 'FIND_IN_SET', formatter: Table.api.formatter.label },
                        { field: 'image', title: __('Image'), formatter: Table.api.formatter.image },
                        { field: 'forumimages', title: __('Forumimages'), formatter: Table.api.formatter.images },
                        { field: 'title', title: __('Title') },
                        { field: 'user_id', title: __('User_id') },
                        { field: 'tel', title: __('Tel') },
                        { field: 'views', title: __('Views') },
                        { field: 'times', title: __('Times') },
                        { field: 'refreshtime', title: __('Refreshtime'), operate: 'RANGE', addclass: 'datetimerange', formatter: Table.api.formatter.datetime },
                        { field: 'createtime', title: __('Createtime'), operate: 'RANGE', addclass: 'datetimerange', formatter: Table.api.formatter.datetime },
                        { field: 'updatetime', title: __('Updatetime'), operate: 'RANGE', addclass: 'datetimerange', formatter: Table.api.formatter.datetime },
                        { field: 'weigh', title: __('Weigh') },
                        { field: 'switch', title: __('Switch'), searchList: { "1": __('Yes'), "0": __('No') }, formatter: Table.api.formatter.toggle },
                        { field: 'status', title: __('Status'), searchList: { "hidden": __('Status hidden'), "normal": __('Status normal'), "deleted": __('Status deleted'), "agreed": __('Status agreed') }, formatter: Table.api.formatter.status },
                        { field: 'state', title: __('State'), searchList: { "0": __('State 0'), "1": __('State 1'), "2": __('State 2') }, formatter: Table.api.formatter.normal },
                        { field: 'user.username', title: __('User.username') },
                        { field: 'useragain.username', title: __('Useragain.username') },
                        { field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate }
                    ]
                ]
            });


            // 绑定TAB事件
            $('.panel-heading a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var field = $(this).closest("ul").data("field");
                var value = $(this).data("value");
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    var filter = {};
                    if (value !== '') {
                        filter[field] = value;
                    }
                    params.filter = JSON.stringify(filter);
                    return params;
                };
                table.bootstrapTable('refresh', {});
                return false;
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});