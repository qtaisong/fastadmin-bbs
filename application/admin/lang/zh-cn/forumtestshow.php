<?php

return [
    'Id'  =>  'ID',
    'Title'  =>  '留言标题',
    'Content'  =>  '留言内容',
    'User_id'  =>  '会员ID',
    'Tel'  =>  '手机号码',
    'Views'  =>  '点击',
    'Times'  =>  '时间',
    'Refreshtime'  =>  '刷新时间(int)',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间',
    'Weigh'  =>  '权重',
    'Status'  =>  '状态',
    'State'  =>  '状态值',
    'State 0'  =>  '禁用',
    'State 1'  =>  '正常',
    'State 2'  =>  '推荐',
    'User.username'  =>  '用户名'
];
