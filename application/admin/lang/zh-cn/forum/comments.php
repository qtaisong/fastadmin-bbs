<?php

return [
    'Id'  =>  'ID',
    'Content'  =>  '评论内容',
    'User_id'  =>  '会员ID',
    'Comment_id'  =>  '评论ID',
    'Views'  =>  '点击',
    'Refreshtime'  =>  '刷新时间(int)',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间',
    'Weigh'  =>  '权重',
    'Status'  =>  '状态',
    'State'  =>  '状态值',
    'State 0'  =>  '禁用',
    'State 1'  =>  '正常',
    'State 2'  =>  '推荐'
];
