<?php

namespace app\admin\model;

use think\Model;

class Blog extends Model
{
    // 表名
    protected $name = 'blog';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    
    // 追加属性
    protected $append = [
        'flag_text',
        'custom_text',
        'refreshtime_text',
        'status_text',
        'state_text'
    ];
    

    protected static function init()
    {
        self::afterInsert(function ($row) {
            $pk = $row->getPk();
            $row->getQuery()->where($pk, $row[$pk])->update(['weigh' => $row[$pk]]);
        });
    }

    
    public function getFlagList()
    {
        return ['hot' => __('Hot'),'index' => __('Index'),'aaa' => __('Aaa'),'FastAdmin' => __('Fastadmin'),'后台框架' => __('后台框架'),'recommend' => __('Recommend'),'new' => __('New'),'fff' => __('Fff')];
    }     

    public function getCustomList()
    {
        return ['ccc' => __('Ccc'),'ddd' => __('Ddd')];
    }     

    public function getStatusList()
    {
        return ['normal' => __('Status normal'),'hidden' => __('Status hidden'),'deleted' => __('Status deleted'),'tom' => __('Status tom'),'tommy' => __('Status tommy')];
    }     

    public function getStateList()
    {
        return ['0' => __('State 0'),'1' => __('State 1'),'2' => __('State 2')];
    }     


    public function getFlagTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['flag']) ? $data['flag'] : '');
        $valueArr = explode(',', $value);
        $list = $this->getFlagList();
        return implode(',', array_intersect_key($list, array_flip($valueArr)));
    }


    public function getCustomTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['custom']) ? $data['custom'] : '');
        $valueArr = explode(',', $value);
        $list = $this->getCustomList();
        return implode(',', array_intersect_key($list, array_flip($valueArr)));
    }


    public function getRefreshtimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['refreshtime']) ? $data['refreshtime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getStatusTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStateTextAttr($value, $data)
    {        
        $value = $value ? $value : (isset($data['state']) ? $data['state'] : '');
        $list = $this->getStateList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    protected function setFlagAttr($value)
    {
        return is_array($value) ? implode(',', $value) : $value;
    }

    protected function setCustomAttr($value)
    {
        return is_array($value) ? implode(',', $value) : $value;
    }

    protected function setRefreshtimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }


}
