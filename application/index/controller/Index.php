<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use app\common\library\Token;
use think\Db;
use app\index\model\Bbsdemo;
use app\index\model\Blog;
use app\index\model\Test;
use app\index\model\Comment;
use think\Request;
use think\Cookie;

class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = 'default';

    public function _initialize()
    {
        parent::_initialize();
    }

    public function testdemo()
    {
        echo Cookie::get('uid');

    }
    public function index()
    {
         
		// 是否为 POST 请求
		if (Request::instance()->isPost()) {

			$title = input('title');
			$content = input('content');
			$list = Bbsdemo::toadd($title,$content);

			if ($list) {
				# code...
				$this->success('留言成功', 'index/index/');
			}
			
		}
    	
 
       $list = Bbsdemo::bbs();
       $this->assign('list', $list);
       return $this->view->fetch();
    }

    public function bbs()
    {
		// 是否为 POST 请求
		if (Request::instance()->isPost()) {

			$title = input('title');
			$content = input('content');
			$list = Bbsdemo::toadd($title,$content);

			if ($list) {
				# code...
				$this->success('留言成功', 'index/index/bbs');
			}
			
		}
        $list = Bbsdemo::bbs();
        
        $this->assign('list', $list);
        return $this->view->fetch();


         
 
    }

    public function bbsview()
    {
    	$id = input('id');

    	// 是否为 POST 请求
		if (Request::instance()->isPost()) {

			 

			$content = input('content');

			$add = Comment::add($id,$content,1);

			if ($add) {
				# code...
				$this->success('评论成功');
			}

		}

		$bbsview = Bbsdemo::bbsview($id);
 

		$this->assign('bbsview', $bbsview);
 
		
        return $this->view->fetch();

    }

    public function blogview(){

    	$id = input('id');

    	// 是否为 POST 请求
		if (Request::instance()->isPost()) {

			 

			$content = input('content');

			$add = Comment::add($id,$content,2);

			if ($add) {
				# code...
				$this->success('评论成功');
			}

		}

    	$blogview = Blog::blogview($id);

    	// dump($blogview);die();

    	$this->assign('blogview', $blogview);
		
        return $this->view->fetch();


    }
    public function blog()
    {
     
        $list = Blog::show();
        $this->assign('list', $list);
        return $this->view->fetch();
         
 
    }

    public function test()
    {
     
        $list = Test::showtest();
        $this->assign('list', $list);
        return $this->view->fetch('index');
         
 
    }



    public function news()
    {
        $newslist = [];
        return jsonp(['newslist' => $newslist, 'new' => count($newslist), 'url' => 'https://www.fastadmin.net?ref=news']);
    }

}
