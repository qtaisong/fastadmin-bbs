<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use app\common\library\Token;
use think\Db;
use app\index\model\Bbsdemo;
use app\index\model\Forum as ForumModel ;
use app\index\model\Blog;
use app\index\model\Test;
use app\index\model\Comment;
use think\Request;
use think\Cookie;
use app\index\model\ForumComments;
use think\Session;

class Forum extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = 'default';

    public function _initialize()
    {
        parent::_initialize();
    }

 
    public function index()
    {
         
    
         
		// 是否为 POST 请求
		if (Request::instance()->isPost()) {

			$title   = input('title');
			$content = input('content');
            $user_id = $this->auth->id;

            if(is_null($user_id)){
                 
                $this->error('请登录','index/user/login');
            }
            if(!strlen($title)){
                 
                $this->error('标题不能为空');
            }


             

            
			$list = ForumModel::add($title,$content,$user_id);

			if ($list) {
				# code...
				$this->success('发帖成功', 'index/forum/index');
			}
			
		}
    	
 
       $list = ForumModel::show();
       $this->assign('list', $list);
       return $this->view->fetch();
     
    }
    public function view()
    {
    	$id = input('id');

    	// 是否为 POST 请求
		if (Request::instance()->isPost()) {

			 

            $content = input('content');
            
            $user_id = $this->auth->id;

            if(is_null($user_id)){
                 
                $this->error('请登录','index/user/login');
            }
            if(!strlen($content)){
                 
                $this->error('内容不能为空');
            }


			$add = ForumComments::add($id,$content,$user_id);

			if ($add) {
				# code...
				$this->success('评论成功');
			}

		}

		$view = ForumModel::view($id);
 

		$this->assign('view', $view);
 
		
        return $this->view->fetch();

    }

 

}
