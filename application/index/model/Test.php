<?php

namespace app\index\model;

use think\Model;

class Test extends Model
{
 
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    
 
    

    public static function showtest()
    {
        $list = Test::order('id desc')->paginate(5); 
        return $list;
    }
 


    public function getcreatetimeAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['refreshtime']) ? $data['refreshtime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

 


}
