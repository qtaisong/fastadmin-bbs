<?php

namespace app\index\model;

use think\Model;
use think\Session;

class Forum extends Model
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    /**
     * 留言板模型
     * @author guomengtao
     */

    // 获取器修改时间格式
    public function getcreatetimeAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['refreshtime']) ? $data['createtime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }
    public static function show()
    {
        // 查询状态为1的用户数据 并且每页显示10条数据
        $list = Forum::order('id desc')
        // ->paginate(10, false, [
        // 'page'=>input('param.page')?:1,
        //   'type'     => 'app/bootstrapNew',
        //     'var_page' => 'page',
        // ]);
        ->paginate(10);
        return $list;
    }

 
    public static function view($id)
    {

        $view = Forum::find($id);

 
        
        /* 
         * 重要的思路，实现对关联中的数据筛选
         * 直接在模型里书写，方便了复用和管理
         * 通过这里直接调用一对多模型 
        */
       /*  $view->comments = $view->comments()
                    ->where('status','normal')
                    ->order('id desc')
                    ->paginate(5); */
        $view->comments = $view->comments()
                ->order('id desc')
                ->paginate(5);

        return $view;
    }

    public  function comment($value)
    {


        $content = Forum::find($value);
      
        return $content;
    }



    public static function toadd($title,$content)
    {
        $user           = new Forum;
        $user->title     = $title;
        $user->content    = $content;
        $user->save();
        // 获取自增ID
        return $user->id;
    }


    public static function add($title,$content,$user_id)
    {


        $user             = new Forum;
        $user->title      = $title;
        $user->content    = $content;
        $user->user_id    = $user_id;
        $user->save();
        // 获取自增ID
        return $user->id;
    }

    // 重要的模型一对一多关联
    public function comments()
    {
        return $this->hasMany('ForumComments','comment_id','id');
    }


    public function guomengtao()
    {
        return $this->hasMany('Comment','comment_id');
    }


    // 重要的模型一对一对关联
    public function user()
    {
        return $this->hasOne('User','id','user_id');
    }
    


}
