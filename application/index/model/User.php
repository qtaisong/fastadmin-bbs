<?php

namespace app\index\model;

use think\Model;
use think\Session;

class User extends Model
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    /**
     * 留言板模型
     * @author guomengtao
     */
 
    // 获取器修改时间格式
    public function getcreatetimeAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['refreshtime']) ? $data['createtime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }
    public static function self()
    {
        // 查询状态为1的用户数据 并且每页显示10条数据
        $list = Blog::order('id desc')->paginate(5); 
        return $list;
    }

    public static function blogview($id)
    {
         
        $blogview = Blog::get($id); 

        $blogview->blogcomments = $blogview->comments()
                    ->where('category',2)
                    ->where('status','normal')
                    ->order('id desc')
                    ->paginate(5);

        

        
        return $blogview;
    }

    public function comments()
    {
        return $this->hasMany('Comment','comment_id','id');
    }


 

}
