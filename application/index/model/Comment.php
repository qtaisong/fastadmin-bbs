<?php

namespace app\index\model;

use think\Model;
use think\Session;

class Comment extends Model
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    /**
     * 留言板模型
     * @author guomengtao
     */
 
    // 获取器修改时间格式
    public function getcreatetimeAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['refreshtime']) ? $data['createtime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }
    public static function self()
    {
        // 查询状态为1的用户数据 并且每页显示10条数据
        $list = Blog::order('id desc')->paginate(5); 
        return $list;
    }

    public static function view($id)
    {
         
        $view = view::get($id); 
        return $view;
    }

    public static function add($comment_id,$content,$category)
    {
        $user                  = new Comment;
        $user->comment_id      = $comment_id;
        // $user->type            = 7891;
        $user->category        = $category;
        $user->content         = $content;

        $user->save();
        // 获取自增ID
        return $user->id;
    }




 

}
