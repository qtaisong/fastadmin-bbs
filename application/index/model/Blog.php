<?php

namespace app\index\model;

use think\Model;
use think\Session;

class Blog extends Model
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    /**
     * 留言板模型
     * @author guomengtao
     */
 
    // 获取器修改时间格式
    public function getcreatetimeAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['refreshtime']) ? $data['createtime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }
    public static function show()
    {
        // 查询状态为1的用户数据 并且每页显示10条数据
        $result = Blog::order('id desc')->paginate(5);
        
        // $result->commentscount->id = $result->comments()
        //             ->where('category',2)
        //             ->where('status','normal')
        //             ->count();
        
        return $result;
    }

    public static function blogview($id)
    {
         
        $result = Blog::get($id); 

        $result->blogcomments = $result->comments()
                    ->where('category',2)
                    ->where('status','normal')
                    ->order('id desc')
                    ->paginate(5);

        

        
        return $result;
    }

    public function comments()
    {
        return $this->hasMany('Comment','comment_id','id');
    }


 

}
