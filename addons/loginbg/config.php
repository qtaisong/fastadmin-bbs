<?php

return array (
  0 => 
  array (
    'name' => 'mode',
    'title' => '模式',
    'type' => 'radio',
    'content' => 
    array (
      'fixed' => '固定',
      'random' => '每次随机',
      'daily' => '每日切换',
    ),
    'value' => 'random',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  1 => 
  array (
    'name' => 'image',
    'title' => '固定背景图',
    'type' => 'image',
    'content' => 
    array (
    ),
    'value' => '/assets/img/loginbg.jpg',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  2 => 
    array(
        'name'    => 'backupIgnoreTables',
        'title'   => '备份忽略的表',
        'type'    => 'string',
        'content' =>
            array(),
        'value'   => 'fa_admin_log',
        'rule'    => '',
        'msg'     => '',
        'tip'     => '忽略备份的表,多个表以,进行分隔',
        'ok'      => '',
        'extend'  => '',
    ),
);
