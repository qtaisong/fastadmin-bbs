<?php
namespace addons\test;    // 注意命名空间规范

use app\common\library\Menu;
use think\Addons;

/**
 * 插件测试
 */
class Test extends Addons    // 需继承think\addons\Addons类
{
    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
         
        return true;
    }
    
    /**
     * 插件启用方法
     */
    public function enable()
    {
        return true;
    }

    /**
     * 插件禁用方法
     */
    public function disable()
    {
       return true;
    }

    /**
     * 实现的testhook钩子方法
     * @return mixed
     */
    public function testhook($param)
    {
    // 调用钩子时候的参数信息
        dump($param['id']);
    // 当前插件的配置信息，配置信息存在当前目录的config.php文件中，见下方
        dump($this->getConfig()['age']);

        dump(get_addon_config('test')['like']);
    // 可以返回模板，模板文件默认读取的为插件目录中的文件。模板名不能为空！
        return $this->fetch('info');
    }
}