<?php

return array (
  0 => 
  array (
    'name' => 'mode',
    'title' => '模式',
    'type' => 'radio',
    'content' => 
    array (
      'fixed' => '固定',
      'random' => '每次随机',
      'daily' => '每日切换',
    ),
    'value' => 'random',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  1 => 
  array (
    'name' => 'image',
    'title' => '选择固定背景图',
    'type' => 'image',
    'content' => 
    array (
    ),
    'value' => '/assets/img/qrcode.png',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  2 => 
  array (
    'name' => 'birthday',
    'title' => '出生日期',
    'type' => 'datetime',
    'content' => 
    array (
    ),
    'value' => '2018-01-01 13:59:29',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  3 => 
  array (
    'name' => 'age',
    'title' => '年龄',
    'type' => 'number',
    'content' => 
    array (
    ),
    'value' => '18',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  4 => 
  array (
    'name' => 'like',
    'title' => '爱好',
    'type' => 'array',
    'content' => 
    array (
    ),
    'value' => 
    array (
      '打球运动' => '篮球 羽毛球',
      '唱歌' => '男高音',
    ),
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
);
