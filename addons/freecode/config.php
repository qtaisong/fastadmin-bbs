<?php

return array (
  0 => 
  array (
    'name' => 'codetext',
    'title' => '自定义代码文本域',
    'type' => 'text',
    'content' => 
    array (
    ),
    'value' => '<span style="color:#15a589">FastAdmin</span>',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  1 => 
  array (
    'name' => 'freecode',
    'title' => '自定义Html代码',
    'type' => 'array',
    'content' => 
    array (
    ),
    'value' => 
    array (
      '底部广告位' => '   <img src="https://cdn.fastadmin.net/assets/img/beian.png" />',
      '在线客服代码' => '<script type=\'text/javascript\'>     (function(m, ei, q, i, a, j, s) {         m[i] = m[i] || function() {             (m[i].a = m[i].a || []).push(arguments)         };         j = ei.createElement(q),             s = ei.getElementsByTagName(q)[0];         j.async = true;         j.charset = \'UTF-8\';         j.src = \'https://static.meiqia.com/dist/meiqia.js?_=t\';         s.parentNode.insertBefore(j, s);     })(window, document, \'script\', \'_MEIQIA\');     _MEIQIA(\'entId\', 5162); </script>',
      'QQ群' => 'QQ群：123456789',
      'CNZZ统计代码' => '<span  style=\'display: none;\' > <script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? "https://" : "http://");document.write(unescape("%3Cspan id=\'cnzz_stat_icon_1275862484\'%3E%3C/span%3E%3Cscript src=\'" + cnzz_protocol + "s5.cnzz.com/stat.php%3Fid%3D1275862484%26show%3Dpic2\' type=\'text/javascript\'%3E%3C/script%3E"));</script> </span>',
      '友情链接' => ' <br /> 友情链接： <a href="https://www.fastadmin.net">FastAdmin</a>   <a href="https://www.fastadmin.net">FastAdmin</a>   <a href="https://www.fastadmin.net">FastAdmin</a>   <a href="https://www.fastadmin.net">FastAdmin</a>   <a href="https://www.fastadmin.net">FastAdmin</a> ',
      'brand' => '<br /> © 2011-2019 FastAdmin.net.All Rights Reserved',
    ),
    'rule' => 'required',
    'msg' => '提示',
    'tip' => 'tip',
    'ok' => 'ok',
    'extend' => 'ex',
  ),
);
