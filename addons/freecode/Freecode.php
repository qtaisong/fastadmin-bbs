<?php

namespace addons\freecode;

use think\Addons;
use think\Request;

/**
 * 登录背景图插件
 */
class Freecode extends Addons
{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }

    public function responseEnd()
    {
        $request = Request::instance();


        if ($request->module() <> "admin" && !$request->isAjax() && !$request->isPost()) {

            

            $config = $this->getConfig();
            
             // echo $config['image'];
             // echo $config['freecode']['brand'];

             // dump($config);

            echo "<div style='text-align:center '>" ;

            echo $config['codetext'];

            foreach ($config['freecode'] as $key => $value) {
                 # code...
                echo  $value  ;

             }

            echo  "</div>";
 
 
        }
    }

    public function indexLoginInit(\think\Request &$request)
    {
        $config = $this->getConfig();
        if ($config['mode'] == 'random' || $config['mode'] == 'daily') {
            $index = $config['mode'] == 'random' ? mt_rand(1, 4000) : date("Ymd") % 4000;
            $background = "http://img.infinitynewtab.com/wallpaper/" . $index . ".jpg";
        } else {
            $background = cdnurl($config['image']);
        }
        \think\View::instance()->assign('background', $background);
    }
}
